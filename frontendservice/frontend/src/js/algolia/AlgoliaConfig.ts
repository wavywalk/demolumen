import algoliasearch from 'algoliasearch'

export const algoliaConfigData = {

    appId: 'FLC890HPOA',
    apiKey: '21a67458155d077c70f2893b4a43fadb',
    indexName: 'hans_fashionnete',

    facets: {
        brand: {
            name: 'brand',
            humanReadableName: 'Brand'
        },
        primaryCategory: {
            name: 'primaryCategory',
            humanReadableName: 'Product type'
        },
        categories: {
            name: 'categories',
            humanReadableName: 'Category'
        },
        color: {
            name: 'color',
            humanReadableName: 'Color'
        },
        price: {
            name: 'price',
            humanReadableName: 'Price'
        },
        isSale: {
            name: 'isSale',
            humanReadableName: 'Sale'
        }
    }
}

export const getFacetsToRetrieve = () => {
    let result: any[] = []
    Object.keys(algoliaConfigData.facets).forEach((it: any)=>{
        const entry = (algoliaConfigData.facets as any)[it]
        if (entry.name) {
            result.push(entry.name)
        } else {
            result = result.concat(entry.names)
        }
    })
    return result
}

export const getSearchClient = () => {
    return algoliasearch(
        algoliaConfigData.appId,
        algoliaConfigData.apiKey
    )
}