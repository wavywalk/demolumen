import {SubscriptionState} from "../libs/statemanagement/SubscriptionState";
import {algoliaConfigData} from "../algolia/AlgoliaConfig"
import {getUnixTime, startOfDay, endOfDay} from "date-fns"

export class GlobalSearchState extends SubscriptionState {

    static instance = new GlobalSearchState()

    searchState: any = {}

    onSearchStateChange = (newState: any) => {
        this.searchState = newState
        this.triggerUpdate()
    }

    refinePrice = (min: any, max: any) => {
        this.searchState = this.searchState ?? {}
        this.searchState.range = this.searchState.range ?? {}
        this.searchState.range.price = {
            min, max
        }
        this.triggerUpdate()
    }

    cleanup = () => {
        this.searchState = {}
    }

}