import {FormState} from "../libs/formhandling/FormState";
import {Show} from "../models/Show";
import {ModelCollection} from "front-model";

export class MovieSearchState extends FormState {

    model!: Show

    searchResult = new ModelCollection<Show>()

    loading = false

    setLoading = (value: boolean) => {
        this.loading = value
        this.triggerUpdate()
    }

    containsInput() {
        return !!this.model.name
    }

    async search() {
        this.searchResult = await Show.search(
            {queryParams: {q: this.model.name}, isLoadingToggle: this.setLoading}
        )
        this.triggerUpdate()
    }
}