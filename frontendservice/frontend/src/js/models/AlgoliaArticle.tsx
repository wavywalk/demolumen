import {BaseModel, Property} from "../libs/frontmodel/src";

export class AlgoliaArticle extends BaseModel {

    @Property
    objectID?: string

    @Property
    image?: string

    @Property
    brand?: string

    @Property
    name?: string

    @Property
    primaryCategory?: string

    @Property
    color?: string

    @Property
    price?: number

    @Property
    description?: string

    @Property
    categories?: string[]

    @Property
    isSale?: boolean

    @Property
    pseudoPrice?: number

}