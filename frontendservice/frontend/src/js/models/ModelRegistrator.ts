import {ModelRegistry} from "../libs/frontmodel/src";
import {Show} from "./Show";



export class ModelRegistrator {
    static run() {
        ModelRegistry.registeredModels = {
            Show
        }
    }
}