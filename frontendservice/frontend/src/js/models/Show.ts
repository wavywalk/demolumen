import {ApiEndpoint, BaseModel, ModelCollection, Property, RequestOptions} from "../libs/frontmodel/src";
import {App} from "../App";

export class Show extends BaseModel {

    @Property
    name?: string

    @Property
    description?: string

    @Property
    image?: string

    @ApiEndpoint('GET', {url: `/api/search/show`})
    static search: (options: RequestOptions) => Promise<ModelCollection<Show>>

    static async afterSearchRequest(options: RequestOptions) {
        return await this.afterIndexRequest(options)
    }

}