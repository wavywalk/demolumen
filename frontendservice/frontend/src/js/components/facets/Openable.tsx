import React, {useState} from "react";
import {Collapse} from "reactstrap";

const Collapsible: React.FC<{text: string, initialOpen?: boolean}> = (props) => {

    const {text, children, initialOpen} = props

    const [isOpen, setIsOpen] = useState(!!initialOpen);

    const toggle = () => setIsOpen(!isOpen);

    return <div>
        <p
            className={'collapsibleLabel'}
            onClick={toggle}
        >
            <span>{text}</span>
            {isOpen
                ? <div className="arrow-up"/>
                : <div className="arrow-down"/>
            }
        </p>
        <Collapse isOpen={isOpen}>
            {children}
        </Collapse>
    </div>

}

export {Collapsible}