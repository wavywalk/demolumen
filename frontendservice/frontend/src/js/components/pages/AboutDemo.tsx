import React from "react"
import {Container} from "reactstrap"

const AboutDemo = () => {
    return <Container className="page">
        <h1 id="hi-fashionette">Hi Fashionette</h1>
        <p>Implemented your task, but than decided that it&#39;s rather a small thing to make a great impression,
            so I scraped your actual data and written quite a complex search app that uses it and capable of finding e.g.
            &quot;cognac colored smartphone case&quot;.</p>
        <p>Just visit <a href="http://fashionette.wavywalk.com">http://fashionette.wavywalk.com</a></p>
        <p>the fashionette search knockoff is at <a href="http://fashionette.wavywalk.com">http://fashionette.wavywalk.com</a></p>
        <p>the movie search is at <a href="http://fashionette.wavywalk.com/searchShows">http://fashionette.wavywalk.com/searchShows</a></p>
        <h1 id="what-s-included">What&#39;s included</h1>
        <p>Containerized services:<br/>proxy server, backend api, frontend application (clientside app, and assets serving server).</p>
        <p>frontend application is a pure react SPA written in typescript.
            Search was implemented with Algolia.</p>
        <p>Backend is lumen php.</p>
        <p>In backend part you will find the solution for your task, as well as you can actually use an api from the frontend app.</p>
        <p>To access api not from the app, prefix requests path with <code>/api</code> as it&#39;s proxied on that route.</p>
        <h1 id="main-task-movie-api-">Main task (Movie API)</h1>
        <p>lumen backend is in <code>./src</code>
            frontend is in <code>./frontendservice</code></p>
        <p>To run just pull the repo (git clone https://wavywalk@bitbucket.org/wavywalk/demolumen.git) cd to it and run <code>docker-compose up</code> (you need docker installed, ensure that ports 80 and 3000 are not in use on host)</p>
        <p>hit <code>localhost</code> in browser, play with client app, than go to code.</p>
        <p>Never written a line of Lumen so chose it ( and found it quite nice btw.)</p>
        <p>But before you go to app, a bit about file structure, you will see dir in <code>./src/app/Domains</code>
            To navigate in code keep in mind that code structured not against technical purpose (respository, model etc.) but rather on the operational domains.  </p>
        <p>App has two domains the <code>MovieApi</code> and the <code>Show</code>, <code>Show</code> is primary data our application cares about and is central to our business. </p>
        <p>As I&#39;ve written quite of &quot;import/export to/from third party&quot; related code before, task for me is a classical get data from one source
            and transform it to be consumed in another source.</p>
        <p>But first we need to keep in mind a range of potential problems and debt we should avoid:</p>
        <ul>
            <li>We need the types native to our app independent of the types returned from third parties</li>
            <li>We need to be ready to third party&#39;s behaviour changes.</li>
            <li>We may want to reduce reliance to third party, switch to alternative, implement our own, or replace them at runtime.</li>
            <li>Our search API can become potentially complex, single entrypoint should be ready to handle complex input (facets, filtering etc.)</li>
            <li>We need scale in terms of readiness for changes and code maintenance.</li>
        </ul>
        <p>Implemented two solutions:</p>
        <p>Naive approach:
            which you can find in <code>SearchShowNaiveController</code>, that does the job, but in the long run
            it has the drawbacks and may seem to not be ready for aforementioned (later I&#39;ll cover it).</p>
        <p>Optimal approach:
            entry of which you can find in <code>SearchShowController</code>.</p>
        <p>In this approach I tried to cover all the potential problems, and the best way to do that was to implement functionality
            against multiple API&#39;s.</p>
        <p>App depending on config may use either Maze or Omdb api (can be switched at <code>ConfigureDependencies</code>).</p>
        <ul>
            <li><code>Show</code> is the data model (type) that our application cares about (e.g. frontend relies on it).</li>
            <li><code>ShowRepository</code> it&#39;s basically dao responsible for retrieving the <code>Show</code> from any data source
                (e.g. now it&#39;s a api but later we may store it locally etc. so we need it that rest of application is not coupled to a source show&#39;s coming from)</li>
            <li><code>MovieApi</code> is an abstract gateway to the third party (or potentially to our service we implement in future)</li>
            <li><code>MazeApi</code> responsible for communicating with Maze api and shares api interface </li>
            <li><code>OmdbApi</code> responsible for communicating with Omdb api and shares api interface </li>
            <li><code>ShowSearchQueryResolver</code> responsible for transforming query and dispatching to data retrieval entrypoints/</li>
        </ul>
        <p>As a result doesn&#39;t matter which types and with which behaviour apis return data, we can ensure that app uses single point
            to retrieve the necessary type (<code>Show</code>), any change in apis doesn&#39;t break the interface our app relies on. If api changes, we touch code exactly
            in known place and not across all of the codebase.</p>
        <p>As well as we can make things like having result filtering only for one of the providers, e.g. case sensivity related
            filter was implemented only for MazeApi as  well as treat them independently.</p>
        <p>Regarding search, for now app per requirement searches against a title, but what if we add faceting, filtering, etc.?
            To cover that, we have a separate <code>ShowSearchQueryResolver</code> which will handle, transform and dispatch to daos and collect data from them
            corresponding to query parameters supplied.</p>
        <p>With this approach all the problems get covered and app is ready for changes and negates possible technical debt.</p>
        <p>Of course, it&#39;s not the full solution, lots of things missing (reporting, error handling, api dtos, validations etc., configuration)
            but the core is there and other things can be organically added.</p>
        <p>tests are in /tests to run them exec in php container cd to <code>/var/www/html/app</code> and run phpunit</p>
        <p>Of course code is better discussed in person.</p>
    </Container>
}

export {AboutDemo}