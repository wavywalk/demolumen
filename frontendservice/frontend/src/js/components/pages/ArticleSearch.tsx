import {Col, Container, Row} from "reactstrap";
import React from "react";
import {InstantSearch, Configure, SearchBox, CurrentRefinements, Pagination, Stats} from "react-instantsearch-dom";
import {algoliaConfigData, getSearchClient} from "../../algolia/AlgoliaConfig";
import {GlobalSearchState} from "../../states/GlobalSearchState";
import {sanitizeLabel} from "../facets/CurrentRefinements";
import {ArticleHits} from "../article/ArticleHits";
import {ArticleFacetsIndex} from "../article/ArticleFacetsIndex";

const ArticleSearch = () => {

    GlobalSearchState.instance.use()

    return <Container className={'ArticleSearchContainer'}>
        <InstantSearch
            indexName={algoliaConfigData.indexName}
            searchClient={getSearchClient()}
            searchState={GlobalSearchState.instance.searchState}
            onSearchStateChange={GlobalSearchState.instance.onSearchStateChange}
        >
            <Configure
                hitsPerPage={50}
                maxValuesPerFacet={500}
            />
            <Row className="search-box">
                <SearchBox/>
            </Row>
            <Row className="refinements-box">
                <CurrentRefinements
                    clearsQuery={false}
                    transformItems={(e: any)=>{
                        e.map((it: any)=>{
                            it.label = sanitizeLabel(it.label)
                        })
                        return e
                    }}
                />
            </Row>
            <Row>
                <Col lg={3}
                     className="facets-sidebar"
                >
                    <ArticleFacetsIndex/>
                </Col>
                <Col lg={9}>
                    <Row>
                        <Col>
                            <Stats
                                translations={{
                                    stats(nbHits, timeSpentMS) {
                                        return `${nbHits} products`;
                                    },
                                }}
                            />
                        </Col>
                    </Row>
                    <Pagination/>
                    <ArticleHits/>
                    <Pagination/>
                </Col>
            </Row>
        </InstantSearch>
    </Container>
}

export { ArticleSearch }