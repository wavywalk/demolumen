import React, {useMemo} from "react";
import {Button, Container, Row, Spinner} from "reactstrap";
import {Show} from "../../models/Show";
import {MovieSearchState} from "../../states/MovieSearchState";
import {PlainInput} from "../formelements/PlainInput";
import {ShowCard} from "../show/ShowCard";

const MovieSearch: React.FC = () => {

    const searchState = useMemo(()=>{return new MovieSearchState(new Show())}, []).use()

    const onEnterPress = () => {
        searchState.search()
    }

    return <Container>
        <div className={`searchbar`} onKeyDown={(e)=>{
            if (e.keyCode === 13) {
                onEnterPress()
            }
        }}>
            <PlainInput
                formState={searchState}
                model={searchState.model}
                label={'Search movie'}
                property={'name'}
            />
            {searchState.containsInput() &&
                <Button
                    onClick={()=>{
                        searchState.search()
                    }}
                >
                    <span>search</span>
                    {searchState.loading &&
                        <Spinner/>
                    }
                </Button>
            }
        </div>
        <Row>
            {searchState.searchResult.array.map((it)=>{
                return <ShowCard show={it} key={it.name}/>
            })}
        </Row>
    </Container>

}

export {MovieSearch}