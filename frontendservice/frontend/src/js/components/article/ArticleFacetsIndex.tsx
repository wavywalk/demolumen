import React from "react"
import {MenuSelect, RangeSlider, RefinementList} from "react-instantsearch-dom"
import {algoliaConfigData} from "../../algolia/AlgoliaConfig"
import {HierarchicalMenu} from "../facets/HierarchicalMenu"
import {RangeSliderRefinement} from "../facets/RangeSlider"
import {DateRange} from "../facets/DateRangeFacet"
import {SingleSelect} from "../facets/SingleSelect"
import {Collapsible} from "../facets/Openable";


const ArticleFacetsIndex: React.FC = () => {

    const facets = algoliaConfigData.facets

    return <div className="facets-index">
        <div className="menu-select-box price-facet">
            <p>{facets.price.humanReadableName}</p>
            <RangeSliderRefinement
                attribute={facets.price.name}
            />
        </div>
        <div className="menu-select-box">
            <p>{facets.isSale.humanReadableName}</p>
            <RefinementList
                attribute={facets.isSale.name}
            />
        </div>
        <div className="menu-select-box">
            <Collapsible text={facets.brand.humanReadableName}>
                <RefinementList
                    limit={100}
                    attribute={facets.brand.name}
                />
            </Collapsible>
        </div>
        <div className="menu-select-box">
            <Collapsible text={facets.primaryCategory.humanReadableName} initialOpen={true}>
                <RefinementList
                    limit={100}
                    attribute={facets.primaryCategory.name}
                />
            </Collapsible>
        </div>
        <div className="menu-select-box">
            <Collapsible text={facets.categories.humanReadableName}>
                <RefinementList
                    limit={100}
                    attribute={facets.categories.name}
                />
            </Collapsible>
        </div>
        <div className="menu-select-box">
            <Collapsible text={facets.color.humanReadableName}>
                <RefinementList
                    limit={100}
                    attribute={facets.color.name}
                />
            </Collapsible>
        </div>
    </div>
}

export {ArticleFacetsIndex}