import React from "react"
import { connectHits } from "react-instantsearch-dom"
import {AlgoliaArticle} from '../../models/AlgoliaArticle'
import {ModelCollection} from "front-model"
import {ArticleListItem} from "./ArticleListItem";
import {Pagination, Row} from "reactstrap";

const ArticleHitsBase: React.FC<{
    hits: any[]
}> = ({
          hits
      }) => {

    let events = new ModelCollection(...hits.map((it)=>{
        return new AlgoliaArticle(it)
    }))

    return <Row className="hits-index">
        <Pagination/>
        {events.map((it)=>{
            return <ArticleListItem key={it.objectID} article={it}/>
        })}
    </Row>
}

const ArticleHits = connectHits(ArticleHitsBase)

export {ArticleHits}