import * as React from "react";
import {AlgoliaArticle} from "../../models/AlgoliaArticle";
import {Card, CardBody, CardImg, CardText, CardTitle, Col} from "reactstrap";

const ArticleListItem: React.FC<{
    article: AlgoliaArticle
}> = ({article}) => {

    return <Col lg={4}>
        <Card className={'ShowCard'}>
            <CardImg width="100%" src={article.image} />
            <CardBody>
                <CardTitle><h3 className={'title'}>{article.brand}</h3></CardTitle>
                <CardText>
                    <div className={'prices'}>
                        {article.isSale &&
                            <span className='pseudoPrice'>{article.pseudoPrice}</span>
                        }
                        <span className={'price'}>
                            {article.price} €
                        </span>
                    </div>
                </CardText>
            </CardBody>
        </Card>
    </Col>

}

export {ArticleListItem}