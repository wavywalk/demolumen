import React, { useState } from "react";
import {
    NavbarBrand,
    Navbar,
    NavbarToggler,
    Collapse,
    Nav,
    NavItem,
} from "reactstrap"
import { Link, NavLink } from "react-router-dom";

const TopMenu = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar color="light" light expand="md" className="top-menu">
                <NavbarBrand>
                    <NavLink to={'/'} className="nav-link">
                        Home
                    </NavLink>
                </NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink to={`/searchShows`} className="nav-link">
                                Movie API search
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink to={`/aboutDemo`} className="nav-link">
                                ABOUT DEMO
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );
}

export {TopMenu}