import React from "react";
import {Show} from "../../models/Show";
import {Card, CardBody, CardImg, CardText, CardTitle, Col} from "reactstrap";

const ShowCard: React.FC<{show: Show}> = ({show}) => {

    return <Col lg={4}>
        <Card className={'ShowCard'}>
            <CardImg width="100%" src={show.image} alt={show.name} />
            <CardBody>
                <CardTitle>{show.name}</CardTitle>
                <CardText>
                    <div dangerouslySetInnerHTML={{__html: show.description ?? ''}}/>
                </CardText>
            </CardBody>
        </Card>
    </Col>
}

export {ShowCard}