import React from "react"
import { Container } from "reactstrap"
import { TopMenu } from "./menu/TopMenu"
import { Switch, Route } from "react-router"
import {AboutDemo} from "./pages/AboutDemo"
import {MovieSearch} from "./pages/MovieSearch";
import {ArticleSearch} from "./pages/ArticleSearch";

const Main = () => {

    return <Container fluid>
        <TopMenu/>
        <Container fluid>
            <Switch>
                <Route exact path={'/'}>
                    <ArticleSearch/>
                </Route>
                <Route exact path={'/searchShows'}>
                    <MovieSearch/>
                </Route>
                <Route exact path={`/aboutDemo`}>
                    <AboutDemo/>
                </Route>
            </Switch>
        </Container>
    </Container>

}

export {Main}