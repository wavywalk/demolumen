export class NavigationHistoryStore {

    static history: string[] = []

    static pushPath = (path: string) => {
        const history = NavigationHistoryStore.history
        history.push(path)
        if (history.length > 2) {
            history.shift()
        }
        NavigationHistoryStore.history = history
    }



}