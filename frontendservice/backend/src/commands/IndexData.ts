import {AlgoliaConnection} from "../algolia/AlgoliaConnection";
import {getDataToIndex} from "./data";

export class IndexData {

    static run() {
        const index = AlgoliaConnection.getIndexFor('hans_fashionnete')
        index.clearObjects()
        index.saveObjects(getDataToIndex())
    }

}