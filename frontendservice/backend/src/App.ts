import express, {NextFunction, Request, Response} from 'express'
import { Router } from './router'
import {WebpackStatsInfo} from "./assetsmanagement/WebpackStatsInfo";
import "reflect-metadata"
import * as bodyParser from "body-parser"
import cookieParser from "cookie-parser"
import {IndexData} from "./commands/IndexData";

export class App {


    static async init() {
        if (await this.runCommand()) {
            return
        }
        const app = express()
        WebpackStatsInfo.readAndAssignStats()
        app.set('view engine', 'pug')
        app.disable('x-powered-by')
        app.use(express.json())
        app.use(cookieParser())
        app.use(bodyParser.json())
        app.use(this.globalErrorHandler)
        Router.setRoutes(app)
        const port = 3000
        app.listen(port)
        console.log(`listening on ${port}`)
    }

    static async runCommand() {
        let args = process.argv.slice(2)
        const command = args[0] ?? null
        if (!command) {
            return false
        }
        switch (command) {
            case 'index':
                IndexData.run()
        }
        return true
    }

    static globalErrorHandler = (error: Error, req: Request, res: Response, next: NextFunction)=>{
        if (res.headersSent) {
            next(error)
        }
        res.status(500)
        res.render('error', {error})
    }

}