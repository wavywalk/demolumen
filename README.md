# Hi Fashionette

Implemented your task, but than decided that it's rather a small thing to make a great impression,
so I scraped your actual data and written quite a complex search app that uses it and capable of finding e.g. 
"cognac colored smartphone case".

Just visit http://fashionette.wavywalk.com

the fashionette search knockoff is at http://fashionette.wavywalk.com

the movie search is at http://fashionette.wavywalk.com/searchShows

# What's included

Containerized services:   
proxy server, backend api, frontend application (clientside app, and assets serving server).

frontend application is a pure react SPA written in typescript.
Search was implemented with Algolia.

Backend is lumen php.

In backend part you will find the solution for your task, as well as you can actually use an api from the frontend app.

To access api not from the app, prefix requests path with `/api` as it's proxied on that route.

# Main task (Movie API)

lumen backend is in `./src`
frontend is in `./frontendservice`

To run just pull the repo (`git clone https://wavywalk@bitbucket.org/wavywalk/demolumen.git`) cd to it and run `docker-compose up` (you need docker installed, ensure that ports 80 and 3000 are not in use on host)

hit `localhost` in browser, play with client app, than go to code.

Never written a line of Lumen so chose it ( and found it quite nice btw.)

But before you go to app, a bit about file structure, you will see dir in `./src/app/Domains`
To navigate in code keep in mind that code structured not against technical purpose (respository, model etc.) but rather on the operational domains.  

App has two domains the `MovieApi` and the `Show`, `Show` is primary data our application cares about and is central to our business. 

As I've written quite of "import/export to/from third party" related code before, task for me is a classical get data from one source
and transform it to be consumed in another source.

But first we need to keep in mind a range of potential problems and debt we should avoid:
- We need the types native to our app independent of the types returned from third parties
- We need to be ready to third party's behaviour changes.
- We may want to reduce reliance to third party, switch to alternative, implement our own, or replace them at runtime.
- Our search API can become potentially complex, single entrypoint should be ready to handle complex input (facets, filtering etc.)
- We need scale in terms of readiness for changes and code maintenance.

Implemented two solutions:

Naive approach:
which you can find in `SearchShowNaiveController`, that does the job, but in the long run 
it has the drawbacks and may seem to not be ready for aforementioned (later I'll cover it).

Optimal approach:
entry of which you can find in `SearchShowController`.
 
In this approach I tried to cover all the potential problems, and the best way to do that was to implement functionality 
against multiple API's.

App depending on config may use either Maze or Omdb api (can be switched at `ConfigureDependencies`).

* `Show` is the data model (type) that our application cares about (e.g. frontend relies on it).
* `ShowRepository` it's basically dao responsible for retrieving the `Show` from any data source 
(e.g. now it's a api but later we may store it locally etc. so we need it that rest of application is not coupled to a source show's coming from)
* `MovieApi` is an abstract gateway to the third party (or potentially to our service we implement in future)
* `MazeApi` responsible for communicating with Maze api and shares api interface 
* `OmdbApi` responsible for communicating with Omdb api and shares api interface 
* `ShowSearchQueryResolver` responsible for transforming query and dispatching to data retrieval entrypoints/
  
As a result doesn't matter which types and with which behaviour apis return data, we can ensure that app uses single point
to retrieve the necessary type (`Show`), any change in apis doesn't break the interface our app relies on. If api changes, we touch code exactly
in known place and not across all of the codebase.

As well as we can make things like having result filtering only for one of the providers, e.g. case sensivity related
filter was implemented only for MazeApi as  well as treat them independently.

Regarding search, for now app per requirement searches against a title, but what if we add faceting, filtering, etc.? 
To cover that, we have a separate `ShowSearchQueryResolver` which will handle, transform and dispatch to daos and collect data from them
corresponding to query parameters supplied.

With this approach all the problems get covered and app is ready for changes and negates possible technical debt.

Of course, it's not the full solution, lots of things missing (reporting, error handling, api dtos, validations etc., configuration) 
but the core is there and other things can be organically added.

tests are in /tests to run them exec in php container cd to `/var/www/html/app` and run phpunit

Of course code is better discussed in person.
 
  






