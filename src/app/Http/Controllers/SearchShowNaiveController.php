<?php


namespace App\Http\Controllers;


use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Laravel\Lumen\Routing\Controller;

class SearchShowNaiveController extends Controller
{

    public function index(Request $request)
    {
        $query = $request->all()['q'] ?? null;
        if (!$query) {
            return [];
        }
        $inCache = $this->inCache($query);
        if ($inCache) {
            return $inCache;
        }
        $data = $this->getResponseData($query);
        $data = $this->filterNonCaseMatching($data, $query);
        $data = $this->prepareFinalData($data);
        Cache::put($this->getCacheKey($query), $data);
        return $data;
    }

    private function getResponseData($query)
    {
        $client = new Client(['base_uri' => 'http://api.tvmaze.com/']);
        $response = $client->get('search/shows', ['query' => ['q' =>$query]]);
        return json_decode($response->getBody(), true);
    }

    private function filterNonCaseMatching($data, $query)
    {
        return array_filter($data, function($it) use($query) {
            $name = $it['show']['name'] ?? null;
            return strpos($name, $query) !== false;
        });
    }

    private function prepareFinalData($data)
    {
        return array_map(function($it) {
            return $it['show'] ?? [];
        }, $data);
    }

    private function inCache($query)
    {
        return Cache::get($this->getCacheKey($query));
    }

    private function getCacheKey($query)
    {
        return 'search.show.' . $query;
    }
}
