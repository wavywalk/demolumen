<?php


namespace App\Http\Controllers;

use App\Domains\Show\ShowSearchQueryResolver;
use App\Domains\Show\ShowSerializer;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SearchShowController extends Controller
{

    public function index(Request $request, ShowSearchQueryResolver $resolver)
    {
        $query = $request->all();
        $shows = $resolver->resolve($query);
        return ShowSerializer::collectionToArray($shows);
    }

}
