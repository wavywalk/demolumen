<?php


namespace App\Domains\Show;


use App\Domains\MovieApi\AbstractMovieApiDao;
use Illuminate\Support\Facades\Cache;

class ShowRepository
{

    /**
     * @var AbstractMovieApiDao
     */
    private $movieApi;

    public function __construct(AbstractMovieApiDao $movieApi)
    {
        $this->movieApi = $movieApi;
    }

    /**
     * @param $name
     * @return Show[]
     */
    public function searchByName($name)
    {
        if (!$name) {
            return [];
        }
        return $this->movieApi->searchByName($name);
    }

}
