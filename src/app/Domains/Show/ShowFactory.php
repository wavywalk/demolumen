<?php


namespace App\Domains\Show;


use Illuminate\Support\Facades\Log;

class ShowFactory
{

    /**
     * @param $response array
     * @return Show[]
     */
    public static function fromMazeApiResponse($response)
    {
        $result = [];
        foreach ($response as $showData)
        {
            $show = new Show();
            $show->setName($showData['name'] ?? null)
                ->setDescription($showData['summary'] ?? null)
                ->setImage($showData['image']['medium'] ?? null);
            if (!self::isValid($show)) {
                continue;
            }
            $result[] = $show;
        }
        return $result;
    }

    /**
     * @param $response array
     * @return Show[]
     */
    public static function fromOmdbApiResponse(array $response)
    {
        if (empty($response)) {
            return [];
        }
        $show = new Show();
        $show->setName($response['Title'] ?? null)
            ->setDescription($response['Plot'] ?? null)
            ->setImage($response['Poster'] ?? null);
        if (!self::isValid($show)) {
            return [];
        }
        return [$show];
    }

    public static function isValid(Show $show)
    {
        return !!$show->getName();
    }

}
