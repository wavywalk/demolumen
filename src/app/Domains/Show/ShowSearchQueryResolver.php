<?php


namespace App\Domains\Show;


use Illuminate\Support\Facades\Cache;

class ShowSearchQueryResolver
{

    /**
     * @var ShowRepository
     */
    protected $showRepository;

    public function __construct(ShowRepository $showRepository)
    {
        $this->showRepository = $showRepository;
    }

    /**
     * @param $query
     * @return Show[]
     */
    public function resolve($query)
    {
        $name = $query['q'] ?? null;
        if ($name) {
            $cacheKey = 'search.show.byName' . $name;
            return Cache::remember($cacheKey, 5 * 60, function () use ($name) {
                return $this->showRepository->searchByName($name);
            });
        }
        return [];
    }

}
