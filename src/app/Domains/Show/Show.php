<?php


namespace App\Domains\Show;


class Show
{

    /**
     * @var array
     */
    private $data;

    /**
     * Show constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @param string|null $name
     * @return Show
     */
    public function setName(?string $name): Show
    {
        $this->data['name'] = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->data['name'];
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description['description'] ?? null;
    }

    /**
     * @param string|null $description
     * @return Show
     */
    public function setDescription(?string $description)
    {
        $this->data['description'] = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->data['image'];
    }

    /**
     * @param string|null $image
     * @return Show
     */
    public function setImage(?string $image)
    {
        $this->data['image'] = $image;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

}
