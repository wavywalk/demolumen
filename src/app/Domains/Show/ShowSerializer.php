<?php


namespace App\Domains\Show;


class ShowSerializer
{

    /**
     * @param $shows Show[]
     * @return array
     */
    public static function collectionToArray($shows)
    {
        if (empty($shows)) {
            return [];
        }
        return array_map(function($it) {
            return $it->getData();
        }, $shows);
    }

}
