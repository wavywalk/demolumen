<?php


namespace App\Domains\MovieApi;


use App\Domains\Show\Show;
use GuzzleHttp\Client;

abstract class AbstractMovieApiDao
{

    /**
     * @var Client
     */
    protected $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => $this->getBaseUri(),
        ]);
    }

    /**
     * @param $name
     * @return Show[]
     */
    abstract public function searchByName($name);

    /**
     * @return string
     */
    abstract protected function getBaseUri();

    protected function get(string $path, array $requestOptions)
    {
        $response = null;
        $response = $this->httpClient->get($path, $requestOptions);
        return json_decode($response->getBody(), true);
    }

}
