<?php


namespace App\Domains\MovieApi\OmdbApi;


use App\Domains\MovieApi\AbstractMovieApiDao;
use App\Domains\Show\ShowFactory;

class OmdbApi extends AbstractMovieApiDao
{

    protected function getBaseUri()
    {
        return 'http://www.omdbapi.com/';
    }

    protected function get(string $path, array $requestOptions)
    {
        $requestOptions['query']['apiKey'] = '997d234';
        $response = parent::get($path, $requestOptions);
        $failed = $response['Error'] ?? false;
        if ($failed) {
            return [];
        }
        return $response;
    }

    public function searchByName($name)
    {
        $response = $this->get('/', ['query' => ['t' => $name]]);
        return ShowFactory::fromOmdbApiResponse($response);
    }


}
