<?php


namespace App\Domains\MovieApi\TvMazeApi;


class MazeApiFilterResponse
{
    /**
     * @param $responseData array
     * @param $queryName
     * @return array
     */
    public static function execute($responseData, $queryName)
    {
        $result = [];
        foreach ($responseData as $data) {
            $name = $data['show']['name'] ?? null;
            $skip = strpos($name, $queryName) === false;
            if ($skip) {
                continue;
            }
            $result[] = $data['show'];
        };
        return $result;
    }

}
