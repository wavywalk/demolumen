<?php


namespace App\Domains\MovieApi\TvMazeApi;


use App\Domains\MovieApi\AbstractMovieApiDao;
use App\Domains\Show\Show;
use App\Domains\Show\ShowFactory;

class TvMazeApi extends AbstractMovieApiDao
{

    protected function getBaseUri()
    {
        return 'http://api.tvmaze.com/';
    }

    /**
     * @param $name
     * @return Show[]
     */
    public function searchByName($name)
    {
        $response = $this->get('search/shows', ['query' => ['q' => $name]]);
        $filtered = MazeApiFilterResponse::execute($response, $name);
        return ShowFactory::fromMazeApiResponse($filtered);
    }

}
