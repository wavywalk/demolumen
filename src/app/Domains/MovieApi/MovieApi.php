<?php


namespace App\Domains\MovieApi;


use App\Domains\Show\Show;

class MovieApi
{

    private $movieApiDao;


    public function __construct(AbstractMovieApiDao $movieApiDao)
    {
        $this->movieApiDao = $movieApiDao;
    }

    /**
     * @param $name string | null
     * @return Show[]
     */
    public function searchByName($name)
    {
        if (!$name) {
            return [];
        }
        return $this->movieApiDao->searchByName($name);
    }

}
