<?php


namespace App\Dependencies;


use App\Domains\MovieApi\AbstractMovieApiDao;
use App\Domains\MovieApi\OmdbApi\OmdbApi;
use App\Domains\MovieApi\TvMazeApi\TvMazeApi;
use App\Domains\Show\ShowFactory;
use Illuminate\Support\Facades\App;
use Laravel\Lumen\Application;

class ConfigureDependencies
{

    public static function run(Application $app)
    {
        $app->singleton(AbstractMovieApiDao::class, TvMazeApi::class);
    }

}
