<?php


class OmdbApiTest extends TestCase
{

    /**
     * @var \App\Domains\MovieApi\OmdbApi\OmdbApi
     */
    private $api;

    private $existingTitle = 'Redwood';
    private $returnsNotFound = 'ddddddd';

    public function setUp(): void
    {
        parent::setUp();
        $this->api = $this->app->get(\App\Domains\MovieApi\OmdbApi\OmdbApi::class);
    }

    public function testApiRespondsSuccessfully()
    {
        $result = $this->api->searchByName($this->existingTitle);
        $firstShow = $result[0];
        $this->assertInstanceOf(\App\Domains\Show\Show::class, $firstShow);
        $this->assertNotEmpty($firstShow->getName());
    }

    public function testOmdbApiIgnoresCase()
    {
        $correctCase = $this->api->searchByName($this->existingTitle);
        $incorrectCase = $this->api->searchByName(mb_strtolower($this->existingTitle));
        $this->assertEquals(count($correctCase), count($incorrectCase));
    }

    public function testNotFoundTreatedAsEmptyResponse()
    {
        $result = $this->api->searchByName($this->returnsNotFound);
        self::assertEmpty($result);
    }

}
