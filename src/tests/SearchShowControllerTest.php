<?php


class SearchShowControllerTest extends TestCase
{

    public $validResponseQuery = 'Redwood';

    public function testItReturnsValidResponse()
    {
        $content = $this->get('/search/show?q=' . $this->validResponseQuery)
            ->seeStatusCode(200)
            ->response
            ->getContent();
        $content = json_decode($content, true);
        $name = $content[0]['name'] ?? null;
        $this->assertNotEmpty($name);
    }

}
