<?php


class MazeApiDaoTest extends TestCase
{

    /**
     * @var \App\Domains\MovieApi\TvMazeApi\TvMazeApi
     */
    private $api;

    private $existingTitle = 'Redwood Kings';

    public function setUp(): void
    {
        parent::setUp();
        $this->api = $this->app->get(\App\Domains\MovieApi\TvMazeApi\TvMazeApi::class);
    }

    public function testApiRespondsSuccessfully()
    {
        $result = $this->api->searchByName($this->existingTitle);
        $firstShow = $result[0];
        $this->assertInstanceOf(\App\Domains\Show\Show::class, $firstShow);
        $this->assertNotEmpty($firstShow->getName());
    }

    public function testNonMatchingCaseResultsAreFilteredOut()
    {
        $correctCase = $this->api->searchByName($this->existingTitle);
        $incorrectCase = $this->api->searchByName(mb_strtolower($this->existingTitle));
        $this->assertNotEquals(count($correctCase), count($incorrectCase));
    }

}
